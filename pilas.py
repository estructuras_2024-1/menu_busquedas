import tkinter as tk
from tkinter import ttk

class Stack:
    def __init__(self, root):
        self.root = root
        self.items = []
        self.create_widgets()

    def create_widgets(self):
        control_frame = ttk.Frame(self.root)
        control_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        # Entry for adding nodes
        self.node_entry = ttk.Entry(control_frame, width=20)
        self.node_entry.pack(pady=5)
        ttk.Button(control_frame, text="Empilar", command=self.apilar).pack(pady=5)
        ttk.Button(control_frame, text="Desempilar", command=self.desapilar).pack(pady=5)
        ttk.Button(control_frame, text="Último en entrar", command=self.lastIn).pack(pady=5)
        ttk.Button(control_frame, text="Ver Pila", command=self.view).pack(pady=5)

    def view(self):
        print(self.items)

    def empty(self):
        return len(self.items) == 0

    def apilar(self):
        value = self.node_entry.get()
        self.items.append(value)
        self.node_entry.delete(0, tk.END)

    def desapilar(self):
        if self.empty():
            return None
        return self.items.pop()

    def lastIn(self):
        if self.empty():
            return None
        print(self.items[-1])

if __name__ == "__main__":
    root = tk.Tk()
    app = Stack(root)
    root.mainloop()
