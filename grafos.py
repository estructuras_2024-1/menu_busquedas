import tkinter as tk
from tkinter import ttk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
import networkx as nx

class GraphApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Graph Editor")
        
        self.graph = nx.Graph()
        
        self.create_widgets()
        self.draw_graph()

    def create_widgets(self):
        control_frame = ttk.Frame(self.root)
        control_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        # Entry for adding nodes
        self.node_entry = ttk.Entry(control_frame, width=20)
        self.node_entry.pack(pady=5)
        ttk.Button(control_frame, text="Agregar Nodo", command=self.add_node).pack(pady=5)

        # Entries for adding edges
        self.edge_node1_entry = ttk.Entry(control_frame, width=10)
        self.edge_node1_entry.pack(pady=5)
        self.edge_node2_entry = ttk.Entry(control_frame, width=10)
        self.edge_node2_entry.pack(pady=5)
        ttk.Button(control_frame, text="Agregar Arista", command=self.add_edge).pack(pady=5)

        # Entry for removing nodes
        self.remove_node_entry = ttk.Entry(control_frame, width=20)
        self.remove_node_entry.pack(pady=5)
        ttk.Button(control_frame, text="Eliminar Nodo", command=self.remove_node).pack(pady=5)

        # Entries for removing edges
        self.remove_edge_node1_entry = ttk.Entry(control_frame, width=10)
        self.remove_edge_node1_entry.pack(pady=5)
        self.remove_edge_node2_entry = ttk.Entry(control_frame, width=10)
        self.remove_edge_node2_entry.pack(pady=5)
        ttk.Button(control_frame, text="Eliminar Arista", command=self.remove_edge).pack(pady=5)

        # Figure for drawing the graph
        self.figure, self.ax = plt.subplots(figsize=(5, 5))
        self.canvas = FigureCanvasTkAgg(self.figure, master=self.root)
        self.canvas.get_tk_widget().pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)

    def add_node(self):
        node = self.node_entry.get()
        if node:
            self.graph.add_node(node)
            self.node_entry.delete(0, tk.END)
            self.draw_graph()

    def add_edge(self):
        node1 = self.edge_node1_entry.get()
        node2 = self.edge_node2_entry.get()
        if node1 and node2:
            self.graph.add_edge(node1, node2)
            self.edge_node1_entry.delete(0, tk.END)
            self.edge_node2_entry.delete(0, tk.END)
            self.draw_graph()

    def remove_node(self):
        node = self.remove_node_entry.get()
        if node in self.graph:
            self.graph.remove_node(node)
            self.remove_node_entry.delete(0, tk.END)
            self.draw_graph()

    def remove_edge(self):
        node1 = self.remove_edge_node1_entry.get()
        node2 = self.remove_edge_node2_entry.get()
        if self.graph.has_edge(node1, node2):
            self.graph.remove_edge(node1, node2)
            self.remove_edge_node1_entry.delete(0, tk.END)
            self.remove_edge_node2_entry.delete(0, tk.END)
            self.draw_graph()

    def draw_graph(self):
        self.ax.clear()
        pos = nx.spring_layout(self.graph)
        nx.draw(self.graph, pos, ax=self.ax, with_labels=True, node_size=700, node_color="skyblue", font_size=10, font_weight="bold")
        self.canvas.draw()

if __name__ == "__main__":
    root = tk.Tk()
    app = GraphApp(root)
    root.mainloop()
