import tkinter as tk
from tkinter import messagebox
import subprocess
import sys
import os

def viewGrafos():
    # Llamar a opcion1.py con el mismo intérprete de Python
    python_executable = sys.executable
    script_path = os.path.join(os.path.dirname(__file__), 'grafos.py')
    subprocess.Popen([python_executable, script_path])

def viewArbolBinario():
    # Llamar a opcion1.py con el mismo intérprete de Python
    python_executable = sys.executable
    script_path = os.path.join(os.path.dirname(__file__), 'arbolBinario.py')
    subprocess.Popen([python_executable, script_path])

def viewStack():
    python_executable = sys.executable
    script_path = os.path.join(os.path.dirname(__file__), 'pilas.py')
    subprocess.Popen([python_executable, script_path])

def viewQueue():
    python_executable = sys.executable
    script_path = os.path.join(os.path.dirname(__file__), 'colas.py')
    subprocess.Popen([python_executable, script_path])

def main():
    root = tk.Tk()
    root.title("Menú Principal")

    tk.Label(root, text="Selecciona una opción:").pack(pady=10)

    tk.Button(root, text="Pilas", command=viewStack).pack(pady=5)
    tk.Button(root, text="Colas", command=viewQueue).pack(pady=5)
    tk.Button(root, text="Grafos", command=viewGrafos).pack(pady=5)
    tk.Button(root, text="Arbol Binario", command=viewArbolBinario).pack(pady=5)
    tk.Button(root, text="Salir", command=root.quit).pack(pady=5)

    root.mainloop()

if __name__ == "__main__":
    main()
